#run setup
def run(runfile):
  with open(runfile,"r") as rnf:
    exec(rnf.read())
run("C:/work/toshiba/consumption_forecasting/code/config/setup.py")

df = pd.read_csv(path+"""/data/inputs/"""+project_id+"""/"""+project_id+"""_input.csv""")
cols = ['ID','TimeStamp','energy','confidence']
df = df[cols]

ids = pd.DataFrame(df['ID'].unique(),columns=['ID'])

for i in range(0,len(ids)):
  idi = ids['ID'].iloc[i]
  dfi = df[df['ID'] == idi]
  dfi.to_csv(path + """/algorithm/data/platform/consumption_history_store/ID_""" + str(idi) + """.csv""", index=False)
