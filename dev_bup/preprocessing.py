#run setup
def run(runfile):
  with open(runfile,"r") as rnf:
    exec(rnf.read())
run("C:/work/toshiba/consumption_forecasting/algorithm/code/setup.py")

ID = 1001

df = pd.read_csv(path+"""/data/platform/consumption_history_store/ID_"""+str(ID)+""".csv""")
df['DT'] = pd.to_datetime(df['TimeStamp'])
min_date = df['DT'].min()
max_date = df['DT'].max()

#time definitions
df['year'] = df['DT'].dt.year
df['month'] = df['DT'].dt.month
df['day'] = df['DT'].dt.day
df['hour'] = df['DT'].dt.hour
df['minute'] = df['DT'].dt.minute
df['monthrange'] = df.apply(lambda x: monthrange(x['year'],x['month'])[1],axis=1) #this step is computationally intensive
df['day_of_week'] = pd.DatetimeIndex(df['DT']).dayofweek + 1
df['season'] = 3
df.loc[df['month'].isin([6,7,8,9]), 'season'] = 1
df.loc[df['month'].isin([4,5,10]), 'season'] = 2

df.to_csv(path+"""/data/platform/consumption_history/ConsHist.csv""",index=False)





class ConsHist():
    """write this"""

    def __init__(self, ID):
        """load in dataframe and clean"""
        # define self as the user ID
        self.ID = ID

    def cleanConsHist(self):
        # pull in users consumption history from the consumption history store
        self.df = pd.read_csv(path+"""/data/platform/consumption_history_store/ID_"""+self.ID+""".csv""")
        # time definitions
        self.df['DT'] = pd.to_datetime(self.df['TimeStamp'])
        self.df['year'] = self.df['DT'].dt.year
        self.df['month'] = self.df['DT'].dt.month
        self.df['day'] = self.df['DT'].dt.day
        self.df['hour'] = self.df['DT'].dt.hour
        self.df['minute'] = self.df['DT'].dt.minute
        self.df['monthrange'] = self.df.apply(lambda x: monthrange(x['year'], x['month'])[1],
                                    axis=1)  # this step is computationally intensive
        self.df['day_of_week'] = pd.DatetimeIndex(self.df['DT']).dayofweek + 1
        #self.df['season'] = 3
        #self.df.loc[self.df['month'].isin([6, 7, 8, 9]), 'season'] = 1
        #self.df.loc[self.df['month'].isin([4, 5, 10]), 'season'] = 2

    def getMinDate(self):
        return min(self.df['DT'])

    def getMaxDate(self):
        return max(self.df['DT'])

    def writeToConsHist(self):
        self.df.to_csv(path + """/data/platform/consumption_history/ConsHist.csv""", index=False)