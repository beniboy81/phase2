################################################################
#  SETUP                                                       #
#  Function : Configuration file for a model iteration         #
#  Date : 2018-06-06                                           #
#  Author : Ben Stevens                                        #
################################################################

#import libraries
global project_id,path,pd,np,mt,dt,mp,plt,pdf,mse,r2s,sqrt,DataFrame,monthrange,isleap,sqldf,PdfPages,pp,randint,pysqldf,create_grid
import pandas as pd
import numpy as np
import math as mt
import datetime as dt
import matplotlib as mp
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as pdf
from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import r2_score as r2s
from math import sqrt as sqrt
from pandas import DataFrame as DataFrame
from calendar import monthrange as monthrange
from calendar import isleap as isleap
from pandasql import sqldf as sqldf
from matplotlib.backends.backend_pdf import PdfPages as PdfPages
from random import randint as randint

#define global functions
pysqldf = lambda q: sqldf(q, globals())

path = 'C:/work/toshiba/consumption_forecasting/algorithm'

#this is the end of setup

