from defaults import Defaults
from thresholds import Thresholds

# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/code/dev/setup.py")


class UsageCharacteristics(Defaults,Thresholds):

    def __init__(self, ID, FUEL):
        """"""
        Thresholds.__init__(self, ID, FUEL)
        Defaults.__init__(self, ID, FUEL)

        self.ch = self.ch
        self.tb = self.getThresholdBoundaries()
        self.CONSUMER = self.CONSUMER
        self.Tt = self.Tt
        self.Dd = self.Dd

        ###for debugging partial histories###
        #self.ch = self.ch[(self.ch['date'] > min(self.ch['date']) + dt.timedelta(days=250))]
        ###################


        cf = pd.read_csv(path + '/data/platform/lookups/daily_profile_coefficients/' + str(self.FUEL) + '_profile' + str(self.CONSUMER) + '.csv')
        cf['date'] = cf['date'].apply(lambda x: dt.datetime.strptime(x, '%d/%m/%Y'))
        #cf['date'] = pd.to_datetime(cf['date'])
        cf['date'] = cf['date'].dt.date
        cf = cf[(cf['date'] > max(self.ch['date']) - dt.timedelta(days=365)) & (cf['date'] <= max(self.ch['date']))]

        de = self.ch.groupby(['date','season', 'day_of_week', 'weekday']).agg({'energy': [sum]}).reset_index()
        de.columns = ["_".join(x) for x in de.columns.ravel()]
        de['date'] = de['date_']
        de['season'] = de['season_']
        de['day_of_week'] = de['day_of_week_']
        de['weekday'] = de['weekday_']
        de.drop(columns=['date_', 'season_', 'day_of_week_', 'weekday_'], inplace=True)
        de = pd.merge(de, cf, how='inner', left_on=['date'], right_on=['date'])
        de['season'] = de['season_x']
        de['day_of_week'] = de['day_of_week_x']
        de.drop(columns=['season_x','day_of_week_x','season_y','day_of_week_y'], inplace=True)

        Ucl = {
                 'edc_0_0': [[1,2,3],[1,2,3,4,5,6,7]]
                ,'edc_0_1': [[1], [1, 2, 3, 4, 5, 6, 7]]
                ,'edc_0_2': [[2], [1, 2, 3, 4, 5, 6, 7]]
                ,'edc_0_3': [[3], [1, 2, 3, 4, 5, 6, 7]]
                ,'edc_1_0': [[1,2,3], [1]]
                ,'edc_2_0': [[1,2,3], [2]]
                ,'edc_3_0': [[1,2,3], [3]]
                ,'edc_4_0': [[1,2,3], [4]]
                ,'edc_5_0': [[1,2,3], [5]]
                ,'edc_6_0': [[1,2,3], [6]]
                ,'edc_7_0': [[1,2,3], [7]]
                ,'edc_8_0': [[1,2,3], [1, 2, 3, 4, 5]]
                ,'edc_9_0': [[1,2,3], [6, 7]]
                ,'edc_1_1': [[1], [1]]
                ,'edc_2_1': [[1], [2]]
                ,'edc_3_1': [[1], [3]]
                ,'edc_4_1': [[1], [4]]
                ,'edc_5_1': [[1], [5]]
                ,'edc_6_1': [[1], [6]]
                ,'edc_7_1': [[1], [7]]
                ,'edc_8_1': [[1], [1, 2, 3, 4, 5]]
                ,'edc_9_1': [[1], [6, 7]]
                ,'edc_1_2': [[2], [1]]
                ,'edc_2_2': [[2], [2]]
                ,'edc_3_2': [[2], [3]]
                ,'edc_4_2': [[2], [4]]
                ,'edc_5_2': [[2], [5]]
                ,'edc_6_2': [[2], [6]]
                ,'edc_7_2': [[2], [7]]
                ,'edc_8_2': [[2], [1, 2, 3, 4, 5]]
                ,'edc_9_2': [[2], [6, 7]]
                ,'edc_1_3': [[3], [1]]
                ,'edc_2_3': [[3], [2]]
                ,'edc_3_3': [[3], [3]]
                ,'edc_4_3': [[3], [4]]
                ,'edc_5_3': [[3], [5]]
                ,'edc_6_3': [[3], [6]]
                ,'edc_7_3': [[3], [7]]
                ,'edc_8_3': [[3], [1, 2, 3, 4, 5]]
                ,'edc_9_3': [[3], [6, 7]]

              }

        Uc = {}

        for uc in Ucl:

            a_s = Ucl[uc][0]
            a_d = Ucl[uc][1]

            evd = de[(de['season'].isin(a_s)) & (de['day_of_week'].isin(a_d))]['energy_sum'].sum()
            cad = cf[(cf['season'].isin(a_s)) & (cf['day_of_week'].isin(a_d))]['coefficient'].sum()
            cvd = de[(de['season'].isin(a_s)) & (de['day_of_week'].isin(a_d))]['coefficient'].sum()
            ccad = cf[(cf['season'].isin(a_s)) & (cf['day_of_week'].isin(a_d))]['coefficient'].count()

            Uc[uc] = (evd * cad) / (cvd * ccad)

        Uc['ewc_0_0'] = Uc['edc_0_0'] * 7
        Uc['eac_0_0'] = Uc['edc_0_0'] * 365
        Uc['ewc_0_1'] = Uc['edc_0_1'] * 7
        Uc['ewc_0_2'] = Uc['edc_0_2'] * 7
        Uc['ewc_0_3'] = Uc['edc_0_3'] * 7




        Uc1 = {}

        #edc_0_0, ewc_0_0 & eac_0_0
        if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'edc_0_0']['partial_min_data']):
            Uc1['edc_0_0'] = Uc['edc_0_0']
        else:
            Uc1['ewc_0_0'] = self.Dd['ewc_0_0']
        if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'ewc_0_0']['partial_min_data']):
            Uc1['ewc_0_0'] = Uc['ewc_0_0']
        else:
            Uc1['ewc_0_0'] = self.Dd['ewc_0_0']
        if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'eac_0_0']['partial_min_data']):
            Uc1['eac_0_0'] = Uc['eac_0_0']
        else:
            Uc1['eac_0_0'] = self.Dd['eac_0_0']

        #ewc_0_i
        for s in range(1,4):
            s = str(s)
            if self.Tt['0_'+s] >= int(self.tb[self.tb['consumption_characteristic'] == 'ewc_0_'+s]['partial_min_data']):
                Uc1['ewc_0_'+s] = Uc['ewc_0_'+s]
            else:
                Uc1['ewc_0_'+s] = Uc['ewc_0_0']

        #edc_0_i
        for s in range(1,4):
            s = str(s)
            if self.Tt['0_'+s] >= int(self.tb[self.tb['consumption_characteristic'] == 'edc_0_'+s]['partial_min_data']):
                Uc1['edc_0_'+s] = Uc['edc_0_'+s]
            else:
                Uc1['edc_0_'+s] = Uc['edc_0_0']

        #edc_j_0
        for d in range(1,10):
            d = str(d)
            if self.Tt[d+'_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'edc_'+d+'_0']['partial_min_data']):
                Uc1['edc_'+d+'_0'] = Uc['edc_'+d+'_0']
            else:
                Uc1['edc_'+d+'_0'] = Uc['edc_0_0']

        # edc_j_0
        for s in range(1, 4):
            for d in range(1, 10):
                s = str(s)
                d = str(d)
                if self.Tt[d + '_' +s] >= int(self.tb[self.tb['consumption_characteristic'] == 'edc_' + d + '_' +s]['partial_min_data']):
                    Uc1['edc_' + d + '_' +s] = Uc['edc_' + d + '_' +s]
                else:
                    Uc1['edc_' + d + '_' +s] = Uc['edc_' +d+ '_0']

        self.Uc1 = Uc1

        # make dataframe from usage characteristics dictionary and write to usage_characteristics
        uc = pd.DataFrame.from_dict(self.Uc1, orient='index')
        uc.columns = ['energy']
        uc['ExC_weekday_season'] = uc.index
        uc.to_csv(path + """/data/platform/usage_characteristics/""" + str(self.FUEL) + """_usage_characteristics.csv""",index=False)





#print(UsageCharacteristics(1111,'elec').Uc1['edc_8_3'])
#print(UsageCharacteristics(1111,'elec').Uc1['ewc_0_1'])