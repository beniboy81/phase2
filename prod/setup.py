################################################################
#  SETUP                                                       #
#  Function : Configuration file for a model iteration         #
#  Date : 2018-06-06                                           #
#  Author : Ben Stevens                                        #
################################################################

#import libraries
global path,pd,np,mt,dt,DataFrame,monthrange,isleap,randint,gc
import pandas as pd
import numpy as np
import math as mt
import datetime as dt
from pandas import DataFrame as DataFrame
from calendar import monthrange as monthrange
from calendar import isleap as isleap
from random import randint as randint
import gc as gc

path = 'C:/work/toshiba/consumption_forecasting/algorithm'

#this is the end of setup

