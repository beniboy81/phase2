from defaults import Defaults
from thresholds import Thresholds

# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/code/dev/setup.py")

class ProfileCharacteristics(Defaults,Thresholds):

    def __init__(self, ID, FUEL):
        """"""
        Thresholds.__init__(self, ID, FUEL)
        Defaults.__init__(self, ID, FUEL)

        self.ch = self.ch
        self.tb = self.getThresholdBoundaries()
        self.Tt = self.Tt
        self.Dd = self.Dd

        # STAGING AGGREGATIONS

        ws = self.ch[self.ch['f_day_complete'] == 1]

        Pc1 = {}

        #m_0_0

        agg = ws.groupby(['ID', 'month']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1, how='inner', left_on=['ID_'], right_on=['ID__'])
        c['value'] = c['energy_mean'] / (c['energy_mean_sum'] + 0.000000001)
        c['ID'] = c['ID_']
        c['month'] = c['month_']
        c['season'] = 0
        c.drop(columns=['ID_', 'energy_mean', 'ID__', 'energy_mean_sum', 'month_'], inplace=True)
        Pc1['m_0_0'] = c

        #d_0_0
        agg = ws.groupby(['ID', 'day_of_week']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1, how='inner', left_on=['ID_'], right_on=['ID__'])
        c['value'] = c['energy_mean'] / (c['energy_mean_sum'] + 0.000000001)
        c['ID'] = c['ID_']
        c['day'] = c['day_of_week_']
        c['season'] = 0
        c.drop(columns=['ID_', 'energy_mean', 'ID__', 'energy_mean_sum', 'day_of_week_'], inplace=True)
        Pc1['d_0_0'] = c
      
        #d_0_i
        agg = ws.groupby(['ID', 'season', 'day_of_week']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_', 'season_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1, how='inner', left_on=['ID_', 'season_'], right_on=['ID__', 'season__'])
        c['value'] = c['energy_mean'] / (c['energy_mean_sum'] + 0.000000001)
        c['ID'] = c['ID_']
        c['day'] = c['day_of_week_']
        c['season'] = c['season_']
        c.drop(columns=['ID_', 'energy_mean', 'ID__', 'energy_mean_sum', 'day_of_week_', 'season_', 'season__'], inplace=True)
        for s in range(1,4):
            Pc1['d_0_'+str(s)] = c[c['season'] == s]

        #h_0_0
        agg = ws.groupby(['ID','hour','minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1,  how='inner', left_on=['ID_'], right_on = ['ID__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = 0
        c.drop(columns=['ID_','energy_mean','ID__','energy_mean_sum','hour_','minute_'],inplace=True)
        Pc1['h_0_0'] = c
       
        #h_0_i
        agg = ws.groupby(['ID', 'season', 'hour', 'minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_', 'season_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1, how='inner', left_on=['ID_', 'season_'], right_on=['ID__', 'season__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = c['season_']
        c.drop(columns=['ID_', 'energy_mean', 'ID__', 'energy_mean_sum', 'hour_', 'minute_', 'season_', 'season__'],
               inplace=True)
        for s in range(1,4):
            Pc1['h_0_'+str(s)] = c[c['season'] == s]
       
        #h_(1-7)_0
        agg = ws.groupby(['ID','day_of_week','hour','minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_','day_of_week_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1,  how='inner', left_on=['ID_','day_of_week_'], right_on = ['ID__','day_of_week__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['day_of_week'] = c['day_of_week_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = 0
        c.drop(columns=['ID_','day_of_week_','energy_mean','ID__','day_of_week__','energy_mean_sum','hour_','minute_'],inplace=True)
        for d in range(1,8):
            Pc1['h_'+str(d)+'_0'] = c[c['day_of_week'] == d]

        #h_(8-9)_0
        agg = ws.groupby(['ID','weekday','hour','minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_','weekday_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1,  how='inner', left_on=['ID_','weekday_'], right_on = ['ID__','weekday__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['weekday'] = c['weekday_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = 0
        c.drop(columns=['ID_','weekday_','energy_mean','ID__','weekday__','energy_mean_sum','hour_','minute_'],inplace=True)
        Pc1['h_8_0']= c[c['weekday'] == 8]
        Pc1['h_9_0'] = c[c['weekday'] == 9]
       
        #h_(1-7)_i
        agg = ws.groupby(['ID','day_of_week','season','hour','minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_','season_','day_of_week_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1,  how='inner', left_on=['ID_','season_','day_of_week_'], right_on = ['ID__','season__','day_of_week__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['day_of_week'] = c['day_of_week_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = c['season_']
        c.drop(columns=['ID_','energy_mean','ID__','energy_mean_sum','hour_','minute_','season_','season__','day_of_week_','day_of_week__'],inplace=True)
        for s in range(1, 4):
            for d in range(1,8):
                Pc1['h_'+str(d)+'_'+str(s)] = c[(c['day_of_week'] == d) & (c['season'] == s)]

        #h_(8-9)_i
        agg = ws.groupby(['ID','weekday','season','hour','minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_','season_','weekday_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1,  how='inner', left_on=['ID_','season_','weekday_'], right_on = ['ID__','season__','weekday__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['weekday'] = c['weekday_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = c['season_']
        c.drop(columns=['ID_','energy_mean','ID__','energy_mean_sum','hour_','minute_','season_','season__','weekday_','weekday__'],inplace=True)
        for s in range(1,4):
            Pc1['h_8_'+str(s)] = c[(c['weekday'] == 8) & (c['season'] == s)]
            Pc1['h_9_'+str(s)] = c[(c['weekday'] == 9) & (c['season'] == s)]

        # CREATE PROFILE CHARACTERISTICS LOGIC

        Pc = {}
        
        #m
        if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'm_0_0']['partial_min_data']):
            Pc['m_0_0'] = Pc1['m_0_0'].copy(deep=True)
        else:
            Pc['m_0_0'] = self.Dd['m_0_0'].copy(deep=True)

        Pc['m_0_0'].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_m_0_0.csv', index=False)

        #d
        if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'd_0_0']['partial_min_data']):
            Pc['d_0_0'] = Pc1['d_0_0'].copy(deep=True)
        else:
            Pc['d_0_0'] = self.Dd['d_0_0'].copy(deep=True)

        Pc['d_0_0'].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_d_0_0.csv',index=False)

        for s in range(1,4):
            s = str(s)
            if self.Tt['0_'+s] >= int(self.tb[self.tb['consumption_characteristic'] == 'd_0_'+s]['partial_min_data']):
                Pc['d_0_'+s] = Pc1['d_0_'+s].copy(deep=True)
            else:
                Pc['d_0_'+s] = Pc['d_0_0'].copy(deep=True)
                Pc['d_0_'+s]['season'] = int(s)

            Pc['d_0_'+s].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_d_0_'+s+'.csv', index=False)

        #h
        if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_0_0']['partial_min_data']):
            Pc['h_0_0'] = Pc1['h_0_0'].copy(deep=True)
        else:
            Pc['h_0_0'] = self.Dd['h_0_0'].copy(deep=True)

        Pc['h_0_0'].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_h_0_0.csv',index=False)

        for s in range(1,4):
            s = str(s)
            if self.Tt['0_'+s] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_0_'+s]['partial_min_data']):
                Pc['h_0_'+s] = Pc1['h_0_'+s].copy(deep=True)
                Pc['h_0_' + s]['season'] = int(s)
            else:
                Pc['h_0_'+s] = Pc['h_0_0'].copy(deep=True)
                Pc['h_0_'+s]['season'] = int(s)

            Pc['h_0_'+s].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_h_0_'+s+'.csv', index=False)

        for d in range(0,10):
            d = str(d)
            if self.Tt[d+'_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_'+d+'_0']['partial_min_data']):
                Pc['h_'+d+'_0'] = Pc1['h_'+d+'_0'].copy(deep=True)
                Pc['h_' + d + '_0']['day_of_week'] = int(d)
            else:
                Pc['h_'+d+'_0'] = Pc['h_0_0'].copy(deep=True)
                Pc['h_'+d+'_0']['day_of_week'] = int(d)

            Pc['h_'+d+'_0'].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_h_'+d+'_0.csv', index=False)

        for s in range(1, 4):
            s = str(s)
            for d in range(1,10):
                d = str(d)
                if self.Tt[d+'_'+s] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_'+d+'_'+s]['partial_min_data']):
                    Pc['h_'+d+'_'+s] = Pc1['h_'+d+'_'+s].copy(deep=True)
                    Pc['h_0_' + s]['season'] = int(s)
                else:
                    Pc['h_'+d+'_'+s] = Pc['h_'+d+'_0'].copy(deep=True)
                    Pc['h_'+d+'_'+s]['season'] = int(s)

                Pc['h_'+d+'_'+s].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_h_'+d+'_'+s+'.csv',index=False)

        self.Pc = Pc
"""
    def writeToProfileCharacteristics(self):
        # m_0_0
        Pc1['m_0_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_m_0_0.csv', index=False)
        # d_0_0
        Pc1['d_0_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_d_0_0.csv', index=False)
        # d_0_i
        for s in range(1,4):
            s = str(s)
            Pc1['d_0_'+s].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_d_0_'+s+'.csv', index=False)
        # h_0_0
        Pc1['h_0_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_0_0.csv',index=False)
        # h_0_i
        for s in range(1,4):
            s = str(s)
            Pc1['h_0_'+s].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_0_'+s+'.csv',index=False)
        # h_(1-7)_0
        for d in range(1,8):
            d = str(d)
            Pc1['h_'+d+'_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_'+d+'_0.csv',index=False)
        # h_(8-9)_0
        Pc1['h_8_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_8_0.csv',index=False)
        Pc1['h_9_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_9_0.csv',index=False)
        # h_(1-7)_i
        for s in range(1, 4):
            for d in range(1,8):
                s = str(s)
                d = str(d)
                Pc1['h_'+d+'_'+s].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_'+d+'_'+s+'.csv',index=False)
        # h_(8-9)_i
        for s in range(1,4):
            s = str(s)
            Pc1['h_8_'+s].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_8_'+s+'.csv',index=False)
            Pc1['h_9_'+s].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_9_'+s+'.csv',index=False)
"""

