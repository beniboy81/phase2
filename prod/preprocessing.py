from profile import Profile

# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/code/dev/setup.py")

class ConsHist():
    """Process the consumption history, creating datatime attributes. Methods for min and max datetime
    in the history and to write the processed file to consumption_history location"""

    def __init__(self, ID, FUEL):
        """load in dataframe and clean"""
        Profile.__init__(self, ID)
        self.FUEL = FUEL
        # pull in users consumption history from the consumption history store
        self.ch = pd.read_csv(path + """/data/platform/consumption_history_store/""" + str(self.FUEL) + """_ID_""" + str(self.ID) + """.csv""")

        # time definitions
        self.ch['DT'] = pd.to_datetime(self.ch['TimeStamp'])
        self.ch['year'] = self.ch['DT'].dt.year
        self.ch['month'] = self.ch['DT'].dt.month
        self.ch['day'] = self.ch['DT'].dt.day
        self.ch['hour'] = self.ch['DT'].dt.hour
        self.ch['minute'] = self.ch['DT'].dt.minute
        self.ch['monthrange'] = self.ch.apply(lambda x: monthrange(x['year'], x['month'])[1],
                                    axis=1)  # this step is computationally intensive
        self.ch['day_of_week'] = pd.DatetimeIndex(self.ch['DT']).dayofweek + 1
        self.ch['season'] = 3
        self.ch.loc[self.ch['month'].isin([6, 7, 8, 9]), 'season'] = 1
        self.ch.loc[self.ch['month'].isin([4, 5, 10]), 'season'] = 2
        self.ch['weekday'] = 8
        self.ch.loc[self.ch['day_of_week'].isin([6,7]), 'weekday'] = 9
        self.ch['date'] = self.ch['DT'].dt.date

        # period definitions
        self.ch['p_year'] = self.ch['DT'].dt.year
        self.ch['p_month'] = self.ch['DT'].dt.month
        self.ch['p_day'] = self.ch['DT'].dt.day.apply(str) + """_""" + self.ch['p_month'].apply(str)  # just give date here
        self.ch['p_halfhr'] = self.ch['DT'].dt.hour * 2 + (self.ch['DT'].dt.minute / 30).apply(np.floor)

        # forward programming
        # forward horizon definitions {h}
        self.ch = self.ch.sort_values(['ID', 'DT'], ascending=[True, True])
        # year
        self.ch['f_year_iter'] = 0
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['p_year'] == self.ch['p_year'].shift(1))
        self.ch['f_year_iter'] = np.where(mask, 0, 1)
        self.ch['f_h_year'] = self.ch.groupby(['ID'])['f_year_iter'].apply(lambda x: x.cumsum())
        # month
        self.ch['f_month_iter'] = 0
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['p_month'] == self.ch['p_month'].shift(1))
        self.ch['f_month_iter'] = np.where(mask, 0, 1)
        self.ch['f_h_month'] = self.ch.groupby(['ID'])['f_month_iter'].apply(lambda x: x.cumsum())
        # day
        self.ch['f_day_iter'] = 0
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['p_day'] == self.ch['p_day'].shift(1))
        self.ch['f_day_iter'] = np.where(mask, 0, 1)
        self.ch['f_h_day'] = self.ch.groupby(['ID'])['f_day_iter'].apply(lambda x: x.cumsum())
        # halfhr
        self.ch['f_halfhr_iter'] = 0
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['p_halfhr'] == self.ch['p_halfhr'].shift(1))
        self.ch['f_halfhr_iter'] = np.where(mask, 0, 1)
        self.ch['f_h_halfhr'] = self.ch.groupby(['ID'])['f_halfhr_iter'].apply(lambda x: x.cumsum())

        # forward complete definitions
        # year
        self.ch['f_year_complete'] = 0
        mask = (self.ch['ID'] != self.ch['ID'].shift(1)) & (self.ch['f_h_year'] == 1) & (self.ch['hour'] == 0) & (self.ch['minute'] == 0)
        self.ch['f_year_complete'] = np.where(mask, 1, 0)
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['f_h_year'] == 1)
        self.ch['f_year_complete'] = np.where(mask, self.ch['f_year_complete'].shift(1), self.ch['f_year_complete'])
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['f_h_year'] > 1)
        self.ch['f_year_complete'] = np.where(mask, 1, self.ch['f_year_complete'])
        # month
        self.ch['f_month_complete'] = 0
        mask = (self.ch['ID'] != self.ch['ID'].shift(1)) & (self.ch['f_h_month'] == 1) & (self.ch['day'] == 1) & (self.ch['hour'] == 0) & (
                    self.ch['minute'] == 0)
        self.ch['f_month_complete'] = np.where(mask, 1, 0)
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['f_h_month'] == 1)
        self.ch['f_month_complete'] = np.where(mask, self.ch['f_month_complete'].shift(1), self.ch['f_month_complete'])
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['f_h_month'] > 1)
        self.ch['f_month_complete'] = np.where(mask, 1, self.ch['f_month_complete'])
        # day
        self.ch['f_day_complete'] = 0
        mask = (self.ch['ID'] != self.ch['ID'].shift(1)) & (self.ch['f_h_day'] == 1) & (self.ch['hour'] == 0) & (self.ch['minute'] == 0)
        self.ch['f_day_complete'] = np.where(mask, 1, 0)
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['f_h_day'] == 1)
        self.ch['f_day_complete'] = np.where(mask, self.ch['f_day_complete'].shift(1), self.ch['f_day_complete'])
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['f_h_day'] > 1)
        self.ch['f_day_complete'] = np.where(mask, 1, self.ch['f_day_complete'])
        # halfhr
        self.ch['f_halfhr_complete'] = 0
        mask = (self.ch['ID'] != self.ch['ID'].shift(1)) & (self.ch['f_h_halfhr'] == 1) & (self.ch['hour'] == 0) & (self.ch['minute'] == 0)
        self.ch['f_halfhr_complete'] = np.where(mask, 1, 0)
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['f_h_halfhr'] == 1)
        self.ch['f_halfhr_complete'] = np.where(mask, self.ch['f_halfhr_complete'].shift(1), self.ch['f_halfhr_complete'])
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['f_h_halfhr'] > 1)
        self.ch['f_halfhr_complete'] = np.where(mask, 1, self.ch['f_halfhr_complete'])

        # backward programming
        # backward horizon definitions {h}
        self.ch = self.ch.sort_values(['ID', 'DT'], ascending=[True, False])
        # year
        self.ch['b_year_iter'] = 0
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['p_year'] == self.ch['p_year'].shift(1))
        self.ch['b_year_iter'] = np.where(mask, 0, 1)
        self.ch['b_h_year'] = self.ch.groupby(['ID'])['b_year_iter'].apply(lambda x: x.cumsum())
        # month
        self.ch['b_month_iter'] = 0
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['p_month'] == self.ch['p_month'].shift(1))
        self.ch['b_month_iter'] = np.where(mask, 0, 1)
        self.ch['b_h_month'] = self.ch.groupby(['ID'])['b_month_iter'].apply(lambda x: x.cumsum())
        # day
        self.ch['b_day_iter'] = 0
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['p_day'] == self.ch['p_day'].shift(1))
        self.ch['b_day_iter'] = np.where(mask, 0, 1)
        self.ch['b_h_day'] = self.ch.groupby(['ID'])['b_day_iter'].apply(lambda x: x.cumsum())
        # halfhr
        self.ch['b_halfhr_iter'] = 0
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['p_halfhr'] == self.ch['p_halfhr'].shift(1))
        self.ch['b_halfhr_iter'] = np.where(mask, 0, 1)
        self.ch['b_h_halfhr'] = self.ch.groupby(['ID'])['b_halfhr_iter'].apply(lambda x: x.cumsum())

        # backward complete definitions
        # year
        self.ch['b_year_complete'] = 0
        mask = (self.ch['ID'] != self.ch['ID'].shift(1)) & (self.ch['month'] == 12) & (self.ch['day'] == 31) & (self.ch['hour'] == 23) & (
                    self.ch['minute'] == 30)
        self.ch['b_year_complete'] = np.where(mask, 1, 0)
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['b_h_year'] == 1)
        self.ch['b_year_complete'] = np.where(mask, self.ch['b_year_complete'].shift(1), self.ch['b_year_complete'])
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['b_h_year'] > 1)
        self.ch['b_year_complete'] = np.where(mask, 1, self.ch['b_year_complete'])
        # month
        self.ch['b_month_complete'] = 0
        mask = (self.ch['ID'] != self.ch['ID'].shift(1)) & (self.ch['b_h_month'] == 1) & (self.ch['day'] == self.ch['monthrange']) & (
                    self.ch['hour'] == 23) & (self.ch['minute'] == 30)
        self.ch['b_month_complete'] = np.where(mask, 1, 0)
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['b_h_month'] == 1)
        self.ch['b_month_complete'] = np.where(mask, self.ch['b_month_complete'].shift(1), self.ch['b_month_complete'])
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['b_h_month'] > 1)
        self.ch['b_month_complete'] = np.where(mask, 1, self.ch['b_month_complete'])
        # day
        self.ch['b_day_complete'] = 0
        mask = (self.ch['ID'] != self.ch['ID'].shift(1)) & (self.ch['b_h_day'] == 1) & (self.ch['day'] == self.ch['monthrange']) & (
                    self.ch['hour'] == 23) & (self.ch['minute'] == 30)
        self.ch['b_day_complete'] = np.where(mask, 1, 0)
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['b_h_day'] == 1)
        self.ch['b_day_complete'] = np.where(mask, self.ch['b_day_complete'].shift(1), self.ch['b_day_complete'])
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['b_h_day'] > 1)
        self.ch['b_day_complete'] = np.where(mask, 1, self.ch['b_day_complete'])
        # halfhr
        self.ch['b_halfhr_complete'] = 0
        mask = (self.ch['ID'] != self.ch['ID'].shift(1)) & (self.ch['b_h_halfhr'] == 1) & (self.ch['hour'] == 23) & (self.ch['minute'] == 30)
        self.ch['b_halfhr_complete'] = np.where(mask, 1, 0)
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['b_h_halfhr'] == 1)
        self.ch['b_halfhr_complete'] = np.where(mask, self.ch['b_halfhr_complete'].shift(1), self.ch['b_halfhr_complete'])
        mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['b_h_halfhr'] > 1)
        self.ch['b_halfhr_complete'] = np.where(mask, 1, self.ch['b_halfhr_complete'])

        # history taken up to the end of the last full month (this can be made to be last full day with 'b_day_complete'
        self.ch = self.ch[(self.ch['b_day_complete'] == 1)]

        # ensure first day of history is complete
        self.ch = self.ch[(self.ch['f_day_complete'] == 1)]

        # history taken from up to end of last month to 365 days prior inclusive
        self.ch = self.ch[(self.ch['date'] > max(self.ch['date']) - dt.timedelta(days=365))]

        # reduce to required columns
        cols = ['DT', 'ID', 'year', 'month', 'day', 'hour', 'minute', 'f_year_complete', 'f_month_complete',
                'f_day_complete', 'f_halfhr_complete', 'b_year_complete', 'b_month_complete', 'b_day_complete',
                'b_halfhr_complete', 'energy', 'confidence_score', 'p_year', 'p_month', 'p_day', 'p_halfhr', 'f_h_year',
                'f_h_month', 'f_h_day', 'f_h_halfhr', 'f_h_year', 'b_h_month', 'b_h_day', 'b_h_halfhr', 'monthrange', 'season', 'day_of_week','weekday','date']
        self.ch = self.ch[cols]

    def getMinDate(self):
        return min(self.ch['DT'])

    def getMaxDate(self):
        return max(self.ch['DT'])

    def writeToConsHist(self):
        self.ch.to_csv(path + """/data/platform/consumption_history/""" + str(self.FUEL) + """_ConsHist.csv""", index=False)

#ID = 1001
#FUEL = 'elec'
#CH = ConsHist(ID,FUEL)
#print(CH.df)
#print(CH.getMinDate())
#print(CH.getMaxDate())
#CH.writeToConsHist()