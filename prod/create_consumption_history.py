#run setup
def run(runfile):
  with open(runfile,"r") as rnf:
    exec(rnf.read())
run("C:/work/toshiba/consumption_forecasting/code/config/setup.py")

df = pd.read_csv("""C:/work/toshiba/consumption_forecasting/data/consumption_dev_data/CER/elec/half_hour_cer_elec_1.csv""")
df['TimeStamp'] = df['Timestamp']
df['ID'] = df['id']
df = df[['ID','TimeStamp','energy','confidence_score']]

ids = pd.DataFrame(df['ID'].unique(),columns=['ID'])

for i in range(0,len(ids)):
  idi = ids['ID'].iloc[i]
  dfi = df[df['ID'] == idi]
  dfi.to_csv(path + """/algorithm/data/platform/consumption_history_store/ELEC_ID_""" + str(idi) + """.csv""", index=False)

df = pd.read_csv("""C:/work/toshiba/consumption_forecasting/data/consumption_dev_data/CER/gas/half_hour_cer_gas_1.csv""")
df['TimeStamp'] = df['Timestamp']
df['ID'] = df['id']
df = df[['ID','TimeStamp','energy','confidence_score']]
ids = pd.DataFrame(df['ID'].unique(),columns=['ID'])

for i in range(0,len(ids)):
  idi = ids['ID'].iloc[i]
  dfi = df[df['ID'] == idi]
  dfi.to_csv(path + """/algorithm/data/platform/consumption_history_store/GAS_ID_""" + str(idi) + """.csv""", index=False)
