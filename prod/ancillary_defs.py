# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/code/dev/setup.py")

def garbage_collect(df):
    del df
    df = pd.DataFrame()
    gc.collect()