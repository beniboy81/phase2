from profile_characteristics import ProfileCharacteristics
from usage_characteristics import UsageCharacteristics

# SETUP
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/code/dev/setup.py")



class MatrixBuilder(UsageCharacteristics,ProfileCharacteristics):

    def __init__(self, ID, FUEL, DT):
        """"""
        UsageCharacteristics.__init__(self, ID, FUEL)
        ProfileCharacteristics.__init__(self, ID, FUEL)

        self.Pc = self.Pc
        self.Uc1 = self.Uc1

        self.DT = DT

        Df_grid = {}

        def create_grid(
                  start_year
                , start_month
                , start_day
                , start_hour
                , start_minute
                , end_year
                , end_month
                , end_day
                , end_hour
                , end_minute
        ):

            init = dt.datetime(int(start_year), int(start_month), int(start_day), int(start_hour), int(start_minute))
            fin = dt.datetime(int(end_year), int(end_month), int(end_day), int(end_hour), int(end_minute))
            ls_grid = [init]
            ud = init
            while ud < fin:
                ud += dt.timedelta(0, 30 * 60)
                ls_grid += [ud]
            Df_grid['grid'] = pd.DataFrame(ls_grid, columns=['DT'])
            print(init)
            print(fin)

        dc = pd.DataFrame(['DT'])
        dc['DT'] = self.DT
        dc['DT'] = dc['DT'].apply(lambda x: dt.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
        dc['DT'] = pd.to_datetime(dc['DT'])

        YearStart = dc['DT'].dt.year
        MonthStart = dc['DT'].dt.month
        DayStart = dc['DT'].dt.day
        HourStart = dc['DT'].dt.hour
        MinuteStart = dc['DT'].dt.minute
        YearEnd = dc['DT'].dt.year + 2
        MonthEnd = 12
        DayEnd = 31
        HourEnd = 23
        MinuteEnd = 30
        create_grid(
            YearStart
            , MonthStart
            , DayStart
            , HourStart
            , MinuteStart
            , YearEnd
            , MonthEnd
            , DayEnd
            , HourEnd
            , MinuteEnd
        )

        # time definitions
        df = Df_grid['grid']
        df['year'] = df['DT'].dt.year
        df['month'] = df['DT'].dt.month
        df['day'] = df['DT'].dt.day
        df['hour'] = df['DT'].dt.hour
        df['minute'] = df['DT'].dt.minute
        df['day_of_week'] = pd.DatetimeIndex(df['DT']).dayofweek + 1
        df['season'] = 3
        df.loc[df['month'].isin([6, 7, 8, 9]), 'season'] = 1
        df.loc[df['month'].isin([4, 5, 10]), 'season'] = 2

        # get final versions of coefficients to use in the matrix builder

        #d
        diw = pd.DataFrame()
        for s in range(1,4):
            s = str(s)
            diw = diw.append(self.Pc['d_0_'+s])
        #h
        hid = pd.DataFrame()
        for s in range(1,4):
            for d in range(1,8):
                s = str(s)
                d = str(d)
                hid = hid.append(self.Pc['h_'+d+'_'+s])

        #####for debugging
        #diw.to_csv(path + '/data/platform/profile_characteristics/testd.csv', index=False)
        #hid.to_csv(path + '/data/platform/profile_characteristics/testh.csv', index=False)
        # df.to_csv(path + '/data/platform/profile_characteristics/testdf.csv', index=False)
        # df_ID_y_w.to_csv(path + '/data/platform/profile_characteristics/testdf.csv', index=False)
        ########################

        df_ID_y_w = df[['year', 'season', 'month', 'day', 'day_of_week']].drop_duplicates(['year', 'season', 'month', 'day', 'day_of_week'])
        df_ID_y_w = pd.merge(df_ID_y_w, self.Pc['m_0_0'], how='inner', left_on=['month'], right_on=['month'])
        df_ID_y_w['season'] = df_ID_y_w['season_x']
        df_ID_y_w['cy'] = df_ID_y_w['value']
        df_ID_y_w.drop(columns=['season_x', 'season_y', 'value'], inplace=True)
        df_ID_y_w = pd.merge(df_ID_y_w, diw, how='inner', left_on=['day_of_week', 'season'],right_on=['day', 'season'])
        df_ID_y_w['cw'] = df_ID_y_w['value']
        df_ID_y_w['day'] = df_ID_y_w['day_x']
        df_ID_y_w.drop(columns=['day_x', 'day_y', 'value'], inplace=True)
        df_ID_m_SCw = df_ID_y_w.groupby(['year', 'month'], as_index=False).agg({'cw': [sum]})
        df_ID_m_SCw.columns = ["_".join(x) for x in df_ID_m_SCw.columns.ravel()]
        df_ID_y_w = pd.merge(df_ID_y_w, df_ID_m_SCw, how='inner', left_on=['year', 'month'],
                             right_on=['year_', 'month_'])
        df_ID_y_w.drop(columns=['year_', 'month_'], inplace=True)
        df_ID_y_w['m'] = (df_ID_y_w['cy'] * df_ID_y_w['cw']) / df_ID_y_w['cw_sum']
        df = pd.merge(df, df_ID_y_w, how='inner', left_on=['year', 'month', 'day'],
                      right_on=['year', 'month', 'day'])

        df['season'] = df['season_x']
        df['day_of_week'] = df['day_of_week_x']
        df.drop(columns=['season_x', 'season_y', 'day_of_week_x', 'day_of_week_y'], inplace=True)
        df_ID_y_w = pd.merge(df_ID_y_w,self.Pc['m_0_0'] , how='inner', left_on=['month'], right_on=['month'])
        df_ID_y_w['season'] = df_ID_y_w['season_x']
        df_ID_y_w['cy'] = df_ID_y_w['value']
        df_ID_y_w.drop(columns=['season_x', 'season_y', 'value'], inplace=True)
        df_ID_y_w = pd.merge(df_ID_y_w, diw, how='inner', left_on=['day_of_week', 'season'],right_on=['day', 'season'])
        df_ID_y_w['cw'] = df_ID_y_w['value']
        df_ID_y_w['day'] = df_ID_y_w['day_x']
        df_ID_y_w.drop(columns=['day_x', 'day_y', 'value'], inplace=True)
        df = pd.merge(df, hid, how='inner', left_on=['season', 'day_of_week', 'hour', 'minute'],right_on=['season', 'day_of_week', 'hour', 'minute'])
        df['cd'] = df['value']
        df.drop(columns=['value'], inplace=True)

        # forecast
        df['energy_pred'] = self.Uc1['eac_0_0'] * df['m'] * df['cd']
        df = df[['DT', 'energy_pred']]
        df = df.sort_values(['DT'], ascending=[True])

        df.to_csv(path + """/data/platform/forecast/""" + str(self.FUEL) + """_consumption_forecast.csv""",index=False)



#####for debugging
        #diw.to_csv(path + '/data/platform/profile_characteristics/testd.csv', index=False)
        #hid.to_csv(path + '/data/platform/profile_characteristics/testh.csv', index=False)
        #df.to_csv(path + '/data/platform/profile_characteristics/testdf.csv', index=False)
        #df_ID_y_w.to_csv(path + '/data/platform/profile_characteristics/testdf.csv', index=False)
        ########################