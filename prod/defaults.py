from profile import Profile
import ancillary_defs as ad

# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/code/dev/setup.py")


class Defaults(Profile):

    def __init__(self, ID, FUEL):
        """"""
        Profile.__init__(self, ID)
        self.FUEL = FUEL
        self.CONSUMER = self.getCustType()

        # make dictionary of defaults
        self.Dd = {}
        
        # EAC, EWC & EDC
        self.eac_0_0 = pd.read_csv(path + """/data/platform/defaults_store/eac.csv""")
        self.Dd['eac_0_0'] = int(self.eac_0_0[(self.eac_0_0['consumer'] == self.CONSUMER) & (self.eac_0_0['fuel'] == self.FUEL)]['value'])
        self.Dd['edc_0_0'] = self.Dd['eac_0_0'] / 365
        self.Dd['ewc_0_0'] = self.Dd['edc_0_0'] * 7

        # day default coefficients
        self.day = pd.read_csv(path + """/data/platform/defaults_store/day.csv""")
        for s in range(0,4):
            self.Dd['h_0_'+str(s)] = self.day[(self.day['fuel'] == self.FUEL) & (self.day['consumer'] == self.CONSUMER) & (self.day['season'] == s)]
        # week default coefficients
        self.week = pd.read_csv(path + """/data/platform/defaults_store/week.csv""")
        for s in range(0,4):
            self.Dd['d_0_'+str(s)] = self.week[(self.week['fuel'] == self.FUEL) & (self.week['consumer'] == self.CONSUMER) & (self.week['season'] == s)]
        # year default coefficients
        self.year = pd.read_csv(path + """/data/platform/defaults_store/year.csv""")
        self.Dd['m_0_0'] = self.year[(self.year['fuel'] == self.FUEL) & (self.year['consumer'] == self.CONSUMER)]

        ad.garbage_collect(self.day)
        ad.garbage_collect(self.week)
        ad.garbage_collect(self.year)

    def writeDefaultsToDefaults(self):
        self.Dd['eacdf_0_0'] = pd.DataFrame([self.Dd['eac_0_0']], columns=['eac_0_0'])
        self.Dd['edcdf_0_0'] = pd.DataFrame([self.Dd['edc_0_0']], columns=['edc_0_0'])
        self.Dd['ewcdf_0_0'] = pd.DataFrame([self.Dd['ewc_0_0']], columns=['ewc_0_0'])

        self.Dd['eacdf_0_0'].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_eac_0_0.csv', index=False)
        self.Dd['edcdf_0_0'].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_edc_0_0.csv', index=False)
        self.Dd['ewcdf_0_0'].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_ewc_0_0.csv', index=False)

        for s in range(0,4):
            s = str(s)
            self.Dd['h_0_'+s].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_h_0_'+s+'.csv', index=False)
            self.Dd['d_0_'+s].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_d_0_'+s+'.csv', index=False)
        self.Dd['m_0_0'].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_m_0_0.csv', index=False)

#D = Defaults(ID,FUEL)
#print(D.day)
#print(D.week)
#print(D.year)






