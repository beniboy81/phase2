from preprocessing import ConsHist

# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/code/dev/setup.py")

#ID = 1617
#FUEL = 'gas'

class Thresholds(ConsHist):

    def __init__(self, ID, FUEL):
        """Derive the number of days available in consumption history for each consumption characteristic and specify the default
        thresholds"""
        ConsHist.__init__(self, ID, FUEL)
        self.ch = self.ch

        # make discionary of thresholds
        self.Tt = {}

        #days
        self.Tt['0_0'] = self.ch[(self.ch['f_day_complete'] == 1)]['date'].nunique()

        #season days
        for s in range(1,4):
            self.Tt['0_'+str(s)] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['season'] == s)]['date'].nunique()

        #days of week
        for d in range(1, 8):
            self.Tt[str(d)+'_0'] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'] == d)]['date'].nunique()
        self.Tt['8_0'] = self.Tt['1_0'] + self.Tt['2_0'] + self.Tt['3_0'] + self.Tt['4_0'] + self.Tt['5_0']
        self.Tt['9_0'] = self.Tt['6_0'] + self.Tt['7_0']

        #season day of week
        for s in range(1, 4):
            for d in range(1,8):
                self.Tt[str(d)+'_'+str(s)] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'] == d) & (self.ch['season'] == s)]['date'].nunique()
        self.Tt['8_1'] = self.Tt['1_1'] + self.Tt['2_1'] + self.Tt['3_1'] + self.Tt['4_1'] + self.Tt['5_1']
        self.Tt['9_1'] = self.Tt['6_1'] + self.Tt['7_1']
        self.Tt['8_2'] = self.Tt['1_2'] + self.Tt['2_2'] + self.Tt['3_2'] + self.Tt['4_2'] + self.Tt['5_2']
        self.Tt['9_2'] = self.Tt['6_2'] + self.Tt['7_2']
        self.Tt['8_3'] = self.Tt['1_3'] + self.Tt['2_3'] + self.Tt['3_3'] + self.Tt['4_3'] + self.Tt['5_3']
        self.Tt['9_3'] = self.Tt['6_3'] + self.Tt['7_3']

        # make dataframe from thresholds dictionary
        self.tt = pd.DataFrame.from_dict(self.Tt, orient='index')
        self.tt.columns = ['threshold_boundary']
        self.tt['threshold'] = self.tt.index

    def writeThresholdThresholds(self):
        self.tt.to_csv(path + """/data/platform/thresholds/""" + str(self.FUEL) + """_thresholds.csv""", index=False)

    def getThresholdBoundaries(self):
        self.tb = pd.read_csv(path + """/data/platform/lookups/threshold_boundaries/threshold_boundaries.csv""")
        return self.tb