import ancillary_defs as ad

# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/code/dev/setup.py")


class Profile():
    """Defines the user"""

    def __init__(self, ID):
        """load in dataframe and clean"""
        # define self as the user ID
        self.ID = ID

    def getCustType(self):
        # sme and domestic allocations
        # 1: domestic, 3:sme, 99:other

        self.df1 = pd.read_csv(path + """/data/platform/profile_store/sme_dom_allocations.csv""")

        consumer = int(self.df1[self.df1['ID'] == self.ID]['consumer'])
        if consumer > 2:
            consumer = 99
        if consumer == 2:
            consumer = 3

        return consumer

        ad.garbage_collect(self.df1)



#ID = 1001
#P = Profile(ID)
#print(P.getCustType())
#print(P.ID)
