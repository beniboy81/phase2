from matrix_builder import MatrixBuilder

# SETUP
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/phase2/code/dev/setup.py")

class Forecasts(MatrixBuilder):

    def __init__(self, ID, FUEL, DT):
        """This class builds forecasts at the different granularities by aggregating the matrix. At the same time we
           derive the confidence scores for each forecast granularity"""
        MatrixBuilder.__init__(self, ID, FUEL, DT)

        self.df = self.df
        self.DT = self.DT
        self.Ucs = self.Ucs
        self.Pc = self.Pc
        self.diw = self.diw
        self.hid = self.hid

        # time definitions
        # Derive useful datetime attributes for the algorithm
        self.df['year'] = self.df['DT'].dt.year
        self.df['month'] = self.df['DT'].dt.month
        self.df['date'] = self.df['DT'].dt.date
        self.df['hour'] = self.df['DT'].dt.hour
        self.df['minute'] = self.df['DT'].dt.minute
        self.df['day_of_week'] = pd.DatetimeIndex(self.df['DT']).dayofweek + 1
        self.df['season'] = 3  # heating seasons
        self.df.loc[self.df['month'].isin([6, 7, 8, 9]), 'season'] = 1
        self.df.loc[self.df['month'].isin([4, 5, 10]), 'season'] = 2

        # Dictionary for the forecasts at different granularities
        self.Fc = {}

        # Year
        agg = self.df.groupby(['year']).agg({'energy_pred': ['sum']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg['year'] = agg['year_']
        agg.drop(columns=['year_'], inplace=True)
        # confidence score equal to the confidence score of the eac
        agg['confidence_score'] = self.Ucs['eac_0_0']
        self.Fc['Y'] = agg.copy(deep=True)

        # Month
        agg = self.df.groupby(['year','month']).agg({'energy_pred': ['sum']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg['year'] = agg['year_']
        agg['month'] = agg['month_']
        agg.drop(columns=['year_'], inplace=True)
        agg.drop(columns=['month_'], inplace=True)
        # confidence score equal to the minimum of the eac and monthly coefficients confidence scores
        agg['confidence_score'] = min(min(self.Pc['m_0_0']['confidence_score']),self.Ucs['eac_0_0'])
        self.Fc['M'] = agg.copy(deep=True)

        # Day
        # We bring in season and day of the week so that we can join in the confidence score for the same for the day
        # granularity. The confidence score is the then the minimum of that for the day of the week and season
        # combination and that of the eac coefficients
        agg = self.df.groupby(['season','day_of_week','date']).agg({'energy_pred': ['sum']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg = pd.merge(agg, self.diw, how='inner', left_on=['season_','day_of_week_'], right_on=['season','day'])
        cs1 = self.Ucs['eac_0_0']
        mask = agg['confidence_score'] > cs1
        agg['confidence_score'] = np.where(mask, cs1, agg['confidence_score'])
        agg['date'] = agg['date_']
        agg['energy_pred'] = agg['energy_pred_sum']
        agg.drop(columns=['date_','season','value','season_','day','day_of_week_','energy_pred_sum'], inplace=True)
        self.Fc['D'] = agg.copy(deep=True)

        # Week
        # Week looks at the minimum of the confidence scores in the days that make up the week
        agg = self.Fc['D'].copy(deep=True)
        agg['week'] = self.df['date'] - pd.DateOffset(weekday=0, weeks=1)
        agg = agg.groupby(['week']).agg({'energy_pred': ['sum'],'confidence_score': ['min']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg['week'] = agg['week_']
        agg['energy_pred'] = agg['energy_pred_sum']
        agg['confidence_score'] = agg['confidence_score_min']
        agg.drop(columns=['week_','energy_pred_sum','confidence_score_min'], inplace=True)
        self.Fc['W'] = agg.copy(deep=True)

        # Half Hour
        # We bring in season and day of the week so that we can join in the confidence score for the same for both
        # day and hour of the day. The confidence score is the then the minimum of that for the day of the week and
        # season combination of both day in the week and hour of the day and that of the eac coefficients
        agg = self.df.groupby(['season', 'day_of_week', 'date', 'hour', 'minute', 'daylight_saving']).agg({'energy_pred': ['sum']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg['date'] = agg['date_']
        agg['hour'] = agg['hour_']
        agg['minute'] = agg['minute_']
        agg['season'] = agg['season_']
        agg['day_of_week'] = agg['day_of_week_']
        agg['daylight_saving'] = agg['daylight_saving_']
        agg.drop(columns=['date_', 'hour_', 'minute_', 'season_', 'day_of_week_','daylight_saving_'], inplace=True)
        agg = pd.merge(agg, self.Fc['D'], how='inner', left_on=['date'],right_on=['date'])
        agg['cs2'] = agg['confidence_score']
        agg.drop(columns=['confidence_score'], inplace=True)
        hidagg = self.hid.groupby(['season', 'day_of_week']).agg({'confidence_score': ['min']}).reset_index()
        hidagg.columns = ["_".join(x) for x in hidagg.columns.ravel()]
        agg = pd.merge(agg, hidagg, how='inner', left_on=['season', 'day_of_week'],right_on=['season_', 'day_of_week_'])
        mask = agg['confidence_score_min'] > agg['cs2']
        agg['confidence_score'] = np.where(mask, agg['cs2'], agg['confidence_score_min'])
        agg['energy_pred'] = agg['energy_pred_sum']
        agg.drop(columns=['energy_pred_sum', 'season', 'season_', 'day_of_week', 'day_of_week_', 'cs2', 'confidence_score_min'], inplace=True)
        self.Fc['HH'] = agg.copy(deep=True)

        # Hour
        agg = self.Fc['HH'].copy(deep=True)
        agg = agg.groupby(['date','hour', 'daylight_saving']).agg({'energy_pred': ['sum'], 'confidence_score': ['min']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg['date'] = agg['date_']
        agg['hour'] = agg['hour_']
        agg['daylight_saving'] = agg['daylight_saving_']
        agg['energy_pred'] = agg['energy_pred_sum']
        agg['confidence_score'] = agg['confidence_score_min']
        agg.drop(columns=['date_', 'hour_', 'energy_pred_sum', 'confidence_score_min','daylight_saving_'], inplace=True)
        self.Fc['H'] = agg.copy(deep=True)

        # output the forecast to 'forecast' in the platform
        for fc in self.Fc:
            self.Fc[fc].to_csv(path + '/data/platform/forecast/' + str(self.FUEL) + '_'+fc+'.csv',index=False)



























