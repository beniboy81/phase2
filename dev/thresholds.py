from preprocessing import ConsHist

# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/phase2/code/dev/setup.py")

#ID = 1617
#FUEL = 'gas'

class Thresholds(ConsHist):
    """Each consumption characteristic has a minimum number of days history that are required to be derived
       before a default value is used. This class derives the number of days available in consumption history for
       each combination of periods we will look over for different characteristics and this can act as a lookup
       of thresholds. The nomenclature is straightforward; a day of the week followed by an underscore followed by
       the season defines as day of the week in a season, for example a winter tuesday, as 2_3. This threshold
       will be recorded in a dictionary for reference later in the algorithm, and represents the number of valid
       days in consumption history that meet that period specification. Each of the season and day of the week
       components may be taken to cover 'all period' when set to zero, for example '0_0' defines the period any day
       in any season. The 'writeThresholdThresholds' method converts the dictionary of values to a dataframe
       and writes it to the platform, but this is not necessary for the algorithm to function. A final method
       'getThresholdBoundaries' loads into a dataframe a lookup file that contains for each consumption
       characteristic the threshold values that act as cut-offs for each to be personalised.
       This class also determines what the mean of the daily confidence scores are over the number of days available
       for combinations of periods and loads these into a dictionary 'Cs'. These are used later when generating the
       consumption characteristics
    """
    def __init__(self, ID, FUEL):
        """Inheritance of the ContHist class, making particular use of the processed contact history dataframe 'ch'"""
        ConsHist.__init__(self, ID, FUEL)
        self.ch = self.ch

        # make dictionary of thresholds
        self.Tt = {}

        # make dictionary of mean confidence scores
        self.Cs = {}

        # days
        self.Tt['0_0'] = self.ch[(self.ch['f_day_complete'] == 1)]['date'].nunique()
        self.Cs['0_0'] = self.ch[(self.ch['f_day_complete'] == 1)]['daily_confidence_score'].mean()

        # season days
        for s in range(1,4):
            self.Tt['0_'+str(s)] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['season'] == s)]['date'].nunique()
            self.Cs['0_' + str(s)] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['season'] == s)]['daily_confidence_score'].mean()

        # days of week
        for d in range(1, 8):
            self.Tt[str(d)+'_0'] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'] == d)]['date'].nunique()
            self.Cs[str(d) + '_0'] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'] == d)]['daily_confidence_score'].mean()
        # weekdays and weekends are recorded as day of the week 8 & 9 respectively
        self.Tt['8_0'] = self.Tt['1_0'] + self.Tt['2_0'] + self.Tt['3_0'] + self.Tt['4_0'] + self.Tt['5_0']
        self.Cs['8_0'] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'].isin([1,2,3,4,5]))]['daily_confidence_score'].mean()
        self.Tt['9_0'] = self.Tt['6_0'] + self.Tt['7_0']
        self.Cs['9_0'] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'].isin([6,7]))]['daily_confidence_score'].mean()

        # season day of week
        for s in range(1, 4):
            for d in range(1,8):
                self.Tt[str(d)+'_'+str(s)] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'] == d) & (self.ch['season'] == s)]['date'].nunique()
                self.Cs[str(d) + '_' + str(s)] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'] == d) & (self.ch['season'] == s)]['daily_confidence_score'].mean()
        self.Tt['8_1'] = self.Tt['1_1'] + self.Tt['2_1'] + self.Tt['3_1'] + self.Tt['4_1'] + self.Tt['5_1']
        self.Cs['8_1'] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'].isin([1,2,3,4,5])) & (self.ch['season'] == 1)]['daily_confidence_score'].mean()
        self.Tt['9_1'] = self.Tt['6_1'] + self.Tt['7_1']
        self.Cs['9_1'] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'].isin([6,7])) & (self.ch['season'] == 1)]['daily_confidence_score'].mean()
        self.Tt['8_2'] = self.Tt['1_2'] + self.Tt['2_2'] + self.Tt['3_2'] + self.Tt['4_2'] + self.Tt['5_2']
        self.Cs['8_2'] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'].isin([1,2,3,4,5])) & (self.ch['season'] == 2)]['daily_confidence_score'].mean()
        self.Tt['9_2'] = self.Tt['6_2'] + self.Tt['7_2']
        self.Cs['9_2'] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'].isin([6, 7])) & (self.ch['season'] == 2)]['daily_confidence_score'].mean()
        self.Tt['8_3'] = self.Tt['1_3'] + self.Tt['2_3'] + self.Tt['3_3'] + self.Tt['4_3'] + self.Tt['5_3']
        self.Cs['8_3'] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'].isin([1,2,3,4,5])) & (self.ch['season'] == 3)]['daily_confidence_score'].mean()
        self.Tt['9_3'] = self.Tt['6_3'] + self.Tt['7_3']
        self.Cs['9_3'] = self.ch[(self.ch['f_day_complete'] == 1) & (self.ch['day_of_week'].isin([6, 7])) & (self.ch['season'] == 3)]['daily_confidence_score'].mean()

        # make dataframe from thresholds dictionary
        self.tt = pd.DataFrame.from_dict(self.Tt, orient='index')
        self.tt.columns = ['threshold_boundary']
        self.tt['threshold'] = self.tt.index

    def writeThresholdThresholds(self):
        """Writes the thresholds to 'thresholds' in the platform"""
        self.tt.to_csv(path + """/data/platform/thresholds/""" + str(self.FUEL) + """_thresholds.csv""", index=False)

    def getThresholdBoundaries(self):
        """Load to a dataframe 'tb' the minimum number of days tht are required for each consumptiob characteristic
        to be derived from consumption history instead of default values"""
        self.tb = pd.read_csv(path + """/data/platform/lookups/threshold_boundaries/threshold_boundaries.csv""")
        return self.tb