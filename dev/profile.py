import ancillary_defs as ad

# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/phase2/code/dev/setup.py")


class Profile():
    """This class takes the user ID variable. This is used to define the user. The getCustType method reads in the consumer type
    of the user from a lookup file and produces an attribute. This profile class is an organising class and may be extended to
    capture other attributes of the user.
    """

    def __init__(self, ID):
        # define self as the user ID
        self.ID = ID

    def getCustType(self):
        """returns the users consumer type """
        # sme and domestic allocations
        # 1: domestic, 3:sme, 99:other

        # this file contains allocations of a uer to consumer type. The code in the file is 1 for domestic, 2 for sme
        # & 3 for other. In our system we want 1 as domestic, 3 as sme and 99 and 3 as 99, so there is some logic here
        # to do this
        self.df1 = pd.read_csv(path + """/data/platform/profile_store/sme_dom_allocations.csv""")

        consumer = int(self.df1[self.df1['ID'] == self.ID]['consumer'])
        if consumer > 2:
            consumer = 99
        if consumer == 2:
            consumer = 3

        return consumer

        ad.garbage_collect(self.df1)



#ID = 1001
#P = Profile(ID)
#print(P.getCustType())
#print(P.ID)
