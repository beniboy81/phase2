# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/phase2/code/dev/setup.py")

def garbage_collect(df):
    del df
    df = pd.DataFrame()
    gc.collect()

def cleanup():
    dd = ['consumption_history', 'defaults', 'forecast', 'matrix', 'profile', 'profile_characteristics', 'thresholds', 'usage_characteristics']
    for d in dd:
        DIR = os.listdir(path+'/data/platform/'+d)
        for f in DIR:
            if f.endswith(".csv"):
                os.remove(path+'/data/platform/'+d+'/'+f)