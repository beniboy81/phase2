from profile import Profile

# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/phase2/code/dev/setup.py")

class ConsHist():
    """This class loads in a users half hour granularity history from the 'consumption_history_store' in the platform
       and processes it to produce attributes useful to the algorithm later. This is held in memory in the dataframe
       'ch'.
       The 'writeToConsHist' method writes this to 'consumption_history' in the platform but this is not essential for
       any components of the algorithm. Processing the history generally involves moving through the users history
       sorted in ascending and descending order to produce flags and summaries of these flags as well as employing some
       datetime manipulations. Key attributes are things such as the season, the day of the week, the number of
       hours/days back a period is and whether it is complete or not.
    """

    def __init__(self, ID, FUEL):
        """Inheritance of the profile class, which is token here as no useful attributes are currently used but they
        will be in future iterations. Fuel is a parameter which is passed in the main console.
        """
        Profile.__init__(self, ID)
        self.FUEL = FUEL

        # pull in users consumption history from the consumption history store

        # if the user does not have history then we use convenience data set to one day which will cause all threshold
        # values in the 'Thresholds' class to be less than the threshold boundaries defined in the business requirements
        # and as such the consumption characteristics and forecasts will be built wholley from default values
        # generated in the 'Defaults' class
        my_file = pt(path + """/data/platform/consumption_history_store/""" + str(self.FUEL) + """_ID_""" + str(self.ID) + """.csv""")
        if my_file.is_file():
            self.ch = pd.read_csv(path + """/data/platform/consumption_history_store/""" + str(self.FUEL) + """_ID_""" + str(self.ID) + """.csv""")

            # time definitions
            # Derive useful datetime attributes for the algorithm
            self.ch['DT'] = pd.to_datetime(self.ch['TimeStamp'])
            self.ch['year'] = self.ch['DT'].dt.year
            self.ch['month'] = self.ch['DT'].dt.month
            self.ch['day'] = self.ch['DT'].dt.day
            self.ch['hour'] = self.ch['DT'].dt.hour
            self.ch['minute'] = self.ch['DT'].dt.minute
            self.ch['monthrange'] = self.ch.apply(lambda x: monthrange(x['year'], x['month'])[1],axis=1)  # days in the month. This step is computationally intensive and sub-optimal
            self.ch['day_of_week'] = pd.DatetimeIndex(self.ch['DT']).dayofweek + 1
            self.ch['season'] = 3  # heating seasons
            self.ch.loc[self.ch['month'].isin([6, 7, 8, 9]), 'season'] = 1
            self.ch.loc[self.ch['month'].isin([4, 5, 10]), 'season'] = 2
            self.ch['weekday'] = 8 # define weekdays and weekends, with flag values equal to 8 and 9 respectively
            self.ch.loc[self.ch['day_of_week'].isin([6,7]), 'weekday'] = 9
            self.ch['date'] = self.ch['DT'].dt.date

            # period definitions
            # define a year, a month, a day and a half hour period
            self.ch['p_year'] = self.ch['DT'].dt.year
            self.ch['p_month'] = self.ch['DT'].dt.month
            self.ch['p_day'] = self.ch['DT'].dt.day.apply(str) + """_""" + self.ch['p_month'].apply(str)
            self.ch['p_halfhr'] = self.ch['DT'].dt.hour * 2 + (self.ch['DT'].dt.minute / 30).apply(np.floor)

            # forward programming
            # forward horizon definitions
            # pass through the history from the first datetime to determine for year, month, day and half hour period
            # whether this period is the first, second and so on.
            self.ch = self.ch.sort_values(['ID', 'DT'], ascending=[True, True])
            self.ch['f_day_iter'] = 0
            mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['p_day'] == self.ch['p_day'].shift(1))
            self.ch['f_day_iter'] = np.where(mask, 0, 1)
            self.ch['f_h_day'] = self.ch.groupby(['ID'])['f_day_iter'].apply(lambda x: x.cumsum())

            # forward complete definitions
            # using the previous steps we can determine if each of the periods start at the first half hour interval within
            # it. For example if the first month in the history begins on the 1st day of that month at 16:00 then the
            # forward complete flag will record a zero as the month period in question does not start at the beginning
            # of the month
            self.ch['f_day_complete'] = 0
            mask = (self.ch['ID'] != self.ch['ID'].shift(1)) & (self.ch['f_h_day'] == 1) & (self.ch['hour'] == 0) & (self.ch['minute'] == 0)
            self.ch['f_day_complete'] = np.where(mask, 1, 0)
            mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['f_h_day'] == 1)
            self.ch['f_day_complete'] = np.where(mask, self.ch['f_day_complete'].shift(1), self.ch['f_day_complete'])
            mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['f_h_day'] > 1)
            self.ch['f_day_complete'] = np.where(mask, 1, self.ch['f_day_complete'])

            # backward programming
            # These definitions are analogous to the forward programming with the difference being we pass through the data
            # from the most recent half hour backwards through the history to determine if each period end at the last
            # half hour interval. For example if the latest datetime in the history is 1900 on the last day of the month
            # then the backwards complete definition will record zero because the last day of the month is incomplete

            # backward horizon definitions
            self.ch = self.ch.sort_values(['ID', 'DT'], ascending=[True, False])
            self.ch['b_day_iter'] = 0
            mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['p_day'] == self.ch['p_day'].shift(1))
            self.ch['b_day_iter'] = np.where(mask, 0, 1)
            self.ch['b_h_day'] = self.ch.groupby(['ID'])['b_day_iter'].apply(lambda x: x.cumsum())

            # backward complete definitions
            # day
            self.ch['b_day_complete'] = 0
            mask = (self.ch['ID'] != self.ch['ID'].shift(1)) & (self.ch['b_h_day'] == 1) & (self.ch['day'] == self.ch['monthrange']) & (
                        self.ch['hour'] == 23) & (self.ch['minute'] == 30)
            self.ch['b_day_complete'] = np.where(mask, 1, 0)
            mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['b_h_day'] == 1)
            self.ch['b_day_complete'] = np.where(mask, self.ch['b_day_complete'].shift(1), self.ch['b_day_complete'])
            mask = (self.ch['ID'] == self.ch['ID'].shift(1)) & (self.ch['b_h_day'] > 1)
            self.ch['b_day_complete'] = np.where(mask, 1, self.ch['b_day_complete'])

            # confidence scores
            dc = pd.read_csv(path + """/data/platform/consumption_history_store/daily/""" + str(self.FUEL) + """_ID_""" + str(self.ID) + """.csv""")
            dc['daily_confidence_score'] = dc['confidence_score']
            dc['daily_energy'] = dc['energy']
            dc['date'] = dc['TimeStamp'].apply(lambda x: dt.datetime.strptime(x, '%Y-%m-%d'))
            dc['DT'] = pd.to_datetime(dc['date'])
            dc['date'] = dc['DT'].dt.date
            dc = dc[['date','daily_energy','daily_confidence_score']]

            self.ch = pd.merge(self.ch, dc, how='inner', left_on=['date'],right_on=['date'])

        else:
            self.ch = pd.read_csv(path + """/data/platform/lookups/convenience_data/lessday_ConsHist.csv""")
            self.ch['ID'] = ID
            self.ch['DT'] = pd.to_datetime(self.ch['DT'])
            self.ch['date'] = self.ch['DT'].dt.date

        # if the user does not have a complete day of history then we use convenience data set to one day which will
        # cause all threshold values in the 'Thresholds' class to be less than the threshold boundaries defined in the
        # business requirements and as such the consumption characteristics and forecasts will be built from
        # default values generated in the 'Defaults' class

        mbdc = max(self.ch['b_day_complete'])
        mfdc = max(self.ch['f_day_complete'])
        mc = mbdc + mfdc

        if mc == 2:
            # history taken up to the end of the last full day
            self.ch = self.ch[(self.ch['b_day_complete'] == 1)]

            # ensure first day of history is complete
            self.ch = self.ch[(self.ch['f_day_complete'] == 1)]

        else:
            self.ch = pd.read_csv(path + """/data/platform/lookups/convenience_data/lessday_ConsHist.csv""")
            self.ch['ID'] = ID
            self.ch['DT'] = pd.to_datetime(self.ch['DT'])
            self.ch['date'] = self.ch['DT'].dt.date

        # history taken from up to end of last month to 365 days prior inclusive
        self.ch = self.ch[(self.ch['date'] > max(self.ch['date']) - dt.timedelta(days=365))]

        # reduce to required columns
        cols = ['DT', 'ID', 'year', 'month', 'day', 'hour', 'minute','f_day_complete', 'b_day_complete', 'energy',
                'confidence_score', 'monthrange', 'season', 'day_of_week','weekday','date','daily_confidence_score']
        self.ch = self.ch[cols]

    def getMinDate(self):
        """method to get the last datetime value in the consumption history"""
        return min(self.ch['DT'])

    def getMaxDate(self):
        """method to get the last datetime value in the consumption history"""
        return max(self.ch['DT'])

    def writeToConsHist(self):
        """method to write the processed consumption history to 'consumption_history' in the platform"""
        self.ch.to_csv(path + """/data/platform/consumption_history/""" + str(self.FUEL) + """_ConsHist.csv""", index=False)






#ID = 1001
#FUEL = 'elec'
#CH = ConsHist(ID,FUEL)
#print(CH.ch)
#print(CH.getMinDate())
#print(CH.getMaxDate())
#CH.writeToConsHist()