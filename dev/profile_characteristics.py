from defaults import Defaults
from thresholds import Thresholds

# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/phase2/code/dev/setup.py")

class ProfileCharacteristics(Defaults,Thresholds):
    """This class derives the set of profile consumption characteristics and organises them in a dictionary.
       Each profile is a normalised average energy value over the history grouped by the granularity in question where
       the threshold requirement of history (defined in the 'Thresholds' class) to generate the personalised version.
       Where it is not available, a proxy is used which may be one of the other consumption characteristics, or a
       default (derived in the 'Defaults' class). For example the Winter Monday half hour of the day profile will first
       derive the average consumption for each of the half hours of the day over the users relevant history. It will
       divide this value into the sum across all the averages for all half hours of the day to arrive at a value for
       each half hour of the day that sums to 1 across all half hours of the day.
       The nomenclature is similar to that defined in the 'Thresholds' class. We use a definition consisting of
       3 values separated by underscores. The first defines if the profile is of the '12 months of the year', '7 days of
       the week' or 48 half hours in the day' as 'm','d' and 'h'. The second is the day of the week and the third
       the season. To illustrate 'h_3_1' refers to a 48 hours of the day profile for a winter wednesday.
       The logic to determine whether the profile is generated from personalised history utilises the Thresholds class
       'Th' dictionary and the 'tb' threshold boundaries dataframe. If this version cannot be produced because the
       history is not available then a proxy may be used, which will simply be another personalised characteristics
       generated earlier in the process. Failing this a default profile is used from the Defaults class 'Dd' dictionary.
       A confidence score is also generated for each characteristic. If the history available to the characterisitc
       (given in 'Th') exceeds the mimumum threshold (given in 'tb') then if it exceeds the complete threshold also
       in 'tb' then it recieves the confidence score from 'Cs'. If the above is true except but the complete threshold
       is not exceeded then it recieves a confience score from 'Cs' - 1 and otherwise a confidence score of 3. In the
       case of a characteristic taking it's proxy, the confidence score from the proxy is inherited
    """
    def __init__(self, ID, FUEL):
        """Thresholds & Defaults classes are inherited. We make use of the threshold boundaries, the derived
           minimum days history available for characteristics and the default values
        """
        Thresholds.__init__(self, ID, FUEL)
        Defaults.__init__(self, ID, FUEL)

        self.ch = self.ch
        self.tb = self.getThresholdBoundaries()
        self.Tt = self.Tt
        self.Dd = self.Dd
        self.Cs = self.Cs

        # STAGING AGGREGATIONS

        ws = self.ch[self.ch['f_day_complete'] == 1]

        Pc1 = {}

        #m_0_0

        # This first step is fully commented. It should be that the logic used here translates to the similar step
        # succeeding this

        # Take the mean over the energy values by grouped by granularity (here month) from the history.
        # Housekeeping on the resultant dataframe
        agg = ws.groupby(['ID', 'month']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        # determine the sum of the means over the grouped by granularity. Housekeeping.
        agg1 = agg.groupby(['ID_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        # Merge sum of averages to the averages to calculate a proportion. Housekeeping.
        c = pd.merge(agg, agg1, how='inner', left_on=['ID_'], right_on=['ID__'])
        c['value'] = c['energy_mean'] / (c['energy_mean_sum'] + 0.000000001) # the last term avoids a div/0 error
        c['ID'] = c['ID_']
        c['month'] = c['month_']
        c['season'] = 0
        c.drop(columns=['ID_', 'energy_mean', 'ID__', 'energy_mean_sum', 'month_'], inplace=True)
        # Create a dataframe entry for the characteristic
        Pc1['m_0_0'] = c.copy(deep=True)

        #d_0_0
        agg = ws.groupby(['ID', 'day_of_week']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1, how='inner', left_on=['ID_'], right_on=['ID__'])
        c['value'] = c['energy_mean'] / (c['energy_mean_sum'] + 0.000000001)
        c['ID'] = c['ID_']
        c['day'] = c['day_of_week_']
        c['season'] = 0
        c.drop(columns=['ID_', 'energy_mean', 'ID__', 'energy_mean_sum', 'day_of_week_'], inplace=True)
        Pc1['d_0_0'] = c.copy(deep=True)
      
        #d_0_i
        agg = ws.groupby(['ID', 'season', 'day_of_week']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_', 'season_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1, how='inner', left_on=['ID_', 'season_'], right_on=['ID__', 'season__'])
        c['value'] = c['energy_mean'] / (c['energy_mean_sum'] + 0.000000001)
        c['ID'] = c['ID_']
        c['day'] = c['day_of_week_']
        c['season'] = c['season_']
        c.drop(columns=['ID_', 'energy_mean', 'ID__', 'energy_mean_sum', 'day_of_week_', 'season_', 'season__'], inplace=True)
        for s in range(1,4):
            Pc1['d_0_'+str(s)] = c[c['season'] == s]

        #h_0_0
        agg = ws.groupby(['ID','hour','minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1,  how='inner', left_on=['ID_'], right_on = ['ID__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = 0
        c.drop(columns=['ID_','energy_mean','ID__','energy_mean_sum','hour_','minute_'],inplace=True)
        Pc1['h_0_0'] = c.copy(deep=True)
       
        #h_0_i
        agg = ws.groupby(['ID', 'season', 'hour', 'minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_', 'season_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1, how='inner', left_on=['ID_', 'season_'], right_on=['ID__', 'season__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = c['season_']
        c.drop(columns=['ID_', 'energy_mean', 'ID__', 'energy_mean_sum', 'hour_', 'minute_', 'season_', 'season__'],
               inplace=True)
        for s in range(1,4):
            Pc1['h_0_'+str(s)] = c[c['season'] == s]
       
        #h_(1-7)_0
        agg = ws.groupby(['ID','day_of_week','hour','minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_','day_of_week_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1,  how='inner', left_on=['ID_','day_of_week_'], right_on = ['ID__','day_of_week__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['day_of_week'] = c['day_of_week_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = 0
        c.drop(columns=['ID_','day_of_week_','energy_mean','ID__','day_of_week__','energy_mean_sum','hour_','minute_'],inplace=True)
        for d in range(1,8):
            Pc1['h_'+str(d)+'_0'] = c[c['day_of_week'] == d]

        #h_(8-9)_0
        agg = ws.groupby(['ID','weekday','hour','minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_','weekday_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1,  how='inner', left_on=['ID_','weekday_'], right_on = ['ID__','weekday__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['weekday'] = c['weekday_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = 0
        c.drop(columns=['ID_','weekday_','energy_mean','ID__','weekday__','energy_mean_sum','hour_','minute_'],inplace=True)
        Pc1['h_8_0']= c[c['weekday'] == 8]
        Pc1['h_9_0'] = c[c['weekday'] == 9]
       
        #h_(1-7)_i
        agg = ws.groupby(['ID','day_of_week','season','hour','minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_','season_','day_of_week_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1,  how='inner', left_on=['ID_','season_','day_of_week_'], right_on = ['ID__','season__','day_of_week__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['day_of_week'] = c['day_of_week_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = c['season_']
        c.drop(columns=['ID_','energy_mean','ID__','energy_mean_sum','hour_','minute_','season_','season__','day_of_week_','day_of_week__'],inplace=True)
        for s in range(1, 4):
            for d in range(1,8):
                Pc1['h_'+str(d)+'_'+str(s)] = c[(c['day_of_week'] == d) & (c['season'] == s)]

        #h_(8-9)_i
        agg = ws.groupby(['ID','weekday','season','hour','minute']).agg({'energy': ['mean']}).reset_index()
        agg.columns = ["_".join(x) for x in agg.columns.ravel()]
        agg1 = agg.groupby(['ID_','season_','weekday_']).agg({'energy_mean': ['sum']}).reset_index()
        agg1.columns = ["_".join(x) for x in agg1.columns.ravel()]
        c = pd.merge(agg, agg1,  how='inner', left_on=['ID_','season_','weekday_'], right_on = ['ID__','season__','weekday__'])
        c['value'] = c['energy_mean']/(c['energy_mean_sum']+0.000000001)
        c['ID'] = c['ID_']
        c['weekday'] = c['weekday_']
        c['hour'] = c['hour_']
        c['minute'] = c['minute_']
        c['season'] = c['season_']
        c.drop(columns=['ID_','energy_mean','ID__','energy_mean_sum','hour_','minute_','season_','season__','weekday_','weekday__'],inplace=True)
        for s in range(1,4):
            Pc1['h_8_'+str(s)] = c[(c['weekday'] == 8) & (c['season'] == s)]
            Pc1['h_9_'+str(s)] = c[(c['weekday'] == 9) & (c['season'] == s)]

        # CREATE PROFILE CHARACTERISTICS LOGIC

        # This section is the logic that determines whether a personalised history is used for a characteristic,
        # a proxy or a default and applies that logic to produce the consumption characteristic in question and
        # organise them in a dictionary. The first step in each if statement compares the level of user history
        # available to that characteristic ('Th' dictionary from Thresholds class) against the business requirement
        # in the dataframe 'tb' also from the Thresholds class. Each characteristic is written to
        # 'profile_characteristics' in the platform

        Pc = {}
        
        #m
        if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'm_0_0']['partial_min_data']):
            Pc['m_0_0'] = Pc1['m_0_0'].copy(deep=True)
            if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'm_0_0']['complete_min_data']):
                Pc['m_0_0']['confidence_score'] = round(self.Cs['0_0'])
            else:
                Pc['m_0_0']['confidence_score'] = round(self.Cs['0_0']) - 1
        else:
            # take the default
            Pc['m_0_0'] = self.Dd['m_0_0'].copy(deep=True)
            Pc['m_0_0']['confidence_score'] = 3

        Pc['m_0_0'].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_m_0_0.csv', index=False)

        #d
        if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'd_0_0']['partial_min_data']):
            Pc['d_0_0'] = Pc1['d_0_0'].copy(deep=True)
            if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'd_0_0']['complete_min_data']):
                Pc['d_0_0']['confidence_score'] = round(self.Cs['0_0'])
            else:
                Pc['d_0_0']['confidence_score'] = round(self.Cs['0_0']) - 1
        else:
            # take the default
            Pc['d_0_0'] = self.Dd['d_0_0'].copy(deep=True)
            Pc['d_0_0']['confidence_score'] = 3

        Pc['d_0_0'].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_d_0_0.csv',index=False)

        for s in range(1,4):
            s = str(s)
            if self.Tt['0_'+s] >= int(self.tb[self.tb['consumption_characteristic'] == 'd_0_'+s]['partial_min_data']):
                Pc['d_0_'+s] = Pc1['d_0_'+s].copy(deep=True)
                if self.Tt['0_' + s] >= int(self.tb[self.tb['consumption_characteristic'] == 'd_0_' + s]['complete_min_data']):
                    Pc['d_0_' + s]['confidence_score'] = round(self.Cs['0_' + s])
                else:
                    Pc['d_0_' + s]['confidence_score'] = round(self.Cs['0_' + s]) - 1

            else:
                # take the overall weekly. This may already have defaulted to a default in earlier steps
                Pc['d_0_'+s] = Pc['d_0_0'].copy(deep=True)
                Pc['d_0_' + s]['confidence_score'] = 3
                # hard code the season
                Pc['d_0_'+s]['season'] = int(s)

            Pc['d_0_'+s].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_d_0_'+s+'.csv', index=False)

        #h
        if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_0_0']['partial_min_data']):
            Pc['h_0_0'] = Pc1['h_0_0'].copy(deep=True)
            if self.Tt['0_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_0_0']['complete_min_data']):
                Pc['h_0_0']['confidence_score'] = round(self.Cs['0_0'])
            else:
                Pc['h_0_0']['confidence_score'] = round(self.Cs['0_0']) - 1
        else:
            # take the default
            Pc['h_0_0'] = self.Dd['h_0_0'].copy(deep=True)
            Pc['h_0_0']['confidence_score'] = 3

        Pc['h_0_0'].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_h_0_0.csv',index=False)

        for s in range(1,4):
            s = str(s)
            if self.Tt['0_'+s] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_0_'+s]['partial_min_data']):
                Pc['h_0_'+s] = Pc1['h_0_'+s].copy(deep=True)
                Pc['h_0_' + s]['season'] = int(s)
                if self.Tt['0_' + s] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_0_' + s]['complete_min_data']):
                    Pc['h_0_' + s]['confidence_score'] = round(self.Cs['0_' + s])
                else:
                    Pc['h_0_' + s]['confidence_score'] = round(self.Cs['0_' + s]) - 1
            else:
                # take the overall daily
                Pc['h_0_'+s] = Pc['h_0_0'].copy(deep=True)
                # hard code the season
                Pc['h_0_'+s]['season'] = int(s)

            Pc['h_0_'+s].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_h_0_'+s+'.csv', index=False)

        for d in range(0,10):
            d = str(d)
            if self.Tt[d+'_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_'+d+'_0']['partial_min_data']):
                Pc['h_'+d+'_0'] = Pc1['h_'+d+'_0'].copy(deep=True)
                Pc['h_' + d + '_0']['day_of_week'] = int(d)
                if self.Tt[d + '_0'] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_' + d + '_0']['complete_min_data']):
                    Pc['h_' + d + '_0']['confidence_score'] = round(self.Cs[d + '_0'])
                else:
                    Pc['h_' + d + '_0']['confidence_score'] = round(self.Cs[d + '_0']) - 1

            else:
                # default to the overall hourly. This may have defaulted to the default already
                Pc['h_'+d+'_0'] = Pc['h_0_0'].copy(deep=True)
                # hard code the day of the week
                Pc['h_'+d+'_0']['day_of_week'] = int(d)

            Pc['h_'+d+'_0'].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_h_'+d+'_0.csv', index=False)

        for s in range(1, 4):
            s = str(s)
            for d in range(1,10):
                d = str(d)
                if self.Tt[d+'_'+s] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_'+d+'_'+s]['partial_min_data']):
                    Pc['h_'+d+'_'+s] = Pc1['h_'+d+'_'+s].copy(deep=True)
                    Pc['h_'+d+'_'+s]['season'] = int(s)
                    if self.Tt[d + '_' + s] >= int(self.tb[self.tb['consumption_characteristic'] == 'h_' + d + '_' + s]['complete_min_data']):
                        Pc['h_'+d+'_'+s]['confidence_score'] = round(self.Cs[d+'_'+s])
                    else:
                        Pc['h_' + d + '_' + s]['confidence_score'] = round(self.Cs[d + '_' + s]) - 1
                else:
                    # default to the day of the week all season. This may already have defaulted to the default.
                    Pc['h_'+d+'_'+s] = Pc['h_'+d+'_0'].copy(deep=True)
                    # hard code the season
                    Pc['h_'+d+'_'+s]['season'] = int(s)

                Pc['h_'+d+'_'+s].to_csv(path + '/data/platform/profile_characteristics/' + str(self.FUEL) + '_h_'+d+'_'+s+'.csv',index=False)

        self.Pc = Pc
"""
    def writeToProfileCharacteristics(self):
        # m_0_0
        Pc1['m_0_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_m_0_0.csv', index=False)
        # d_0_0
        Pc1['d_0_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_d_0_0.csv', index=False)
        # d_0_i
        for s in range(1,4):
            s = str(s)
            Pc1['d_0_'+s].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_d_0_'+s+'.csv', index=False)
        # h_0_0
        Pc1['h_0_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_0_0.csv',index=False)
        # h_0_i
        for s in range(1,4):
            s = str(s)
            Pc1['h_0_'+s].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_0_'+s+'.csv',index=False)
        # h_(1-7)_0
        for d in range(1,8):
            d = str(d)
            Pc1['h_'+d+'_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_'+d+'_0.csv',index=False)
        # h_(8-9)_0
        Pc1['h_8_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_8_0.csv',index=False)
        Pc1['h_9_0'].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_9_0.csv',index=False)
        # h_(1-7)_i
        for s in range(1, 4):
            for d in range(1,8):
                s = str(s)
                d = str(d)
                Pc1['h_'+d+'_'+s].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_'+d+'_'+s+'.csv',index=False)
        # h_(8-9)_i
        for s in range(1,4):
            s = str(s)
            Pc1['h_8_'+s].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_8_'+s+'.csv',index=False)
            Pc1['h_9_'+s].to_csv(path + '/data/platform/profile_characteristics/profile_characteristics_staging/' + str(self.FUEL) + '_h_9_'+s+'.csv',index=False)
"""

