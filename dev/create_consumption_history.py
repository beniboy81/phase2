#run setup
def run(runfile):
  with open(runfile,"r") as rnf:
    exec(rnf.read())
run("C:/work/toshiba/consumption_forecasting/algorithm/phase2/code/dev/setup.py")

# half hour
df = pd.read_csv("""C:/work/toshiba/consumption_forecasting/data/consumption_dev_data/CER/elec/half_hour_cer_elec_1.csv""")
df['TimeStamp'] = df['Timestamp']
df['ID'] = df['id']
df = df[['ID','TimeStamp','energy','confidence_score']]

ids = pd.DataFrame(df['ID'].unique(),columns=['ID'])

for i in range(0,len(ids)):
  idi = ids['ID'].iloc[i]
  dfi = df[df['ID'] == idi]
  dfi.to_csv(path + """/algorithm/data/platform/consumption_history_store/ELEC_ID_""" + str(idi) + """.csv""", index=False)

df = pd.read_csv("""C:/work/toshiba/consumption_forecasting/data/consumption_dev_data/CER/gas/half_hour_cer_gas_1.csv""")
df['TimeStamp'] = df['Timestamp']
df['ID'] = df['id']
df = df[['ID','TimeStamp','energy','confidence_score']]
ids = pd.DataFrame(df['ID'].unique(),columns=['ID'])

for i in range(0,len(ids)):
  idi = ids['ID'].iloc[i]
  dfi = df[df['ID'] == idi]
  dfi.to_csv(path + """/algorithm/data/platform/consumption_history_store/GAS_ID_""" + str(idi) + """.csv""", index=False)

# daily
df = pd.read_csv("""C:/work/toshiba/consumption_forecasting/data/consumption_dev_data/CER/elec/daily_cer_elec_1.csv""")
df['TimeStamp'] = df['Timestamp']
df['ID'] = df['id']
df = df[['ID','TimeStamp','energy','confidence_score']]

ids = pd.DataFrame(df['ID'].unique(),columns=['ID'])

for i in range(0,len(ids)):
  idi = ids['ID'].iloc[i]
  dfi = df[df['ID'] == idi]
  dfi.to_csv(path + """/algorithm/data/platform/consumption_history_store/daily/ELEC_ID_""" + str(idi) + """.csv""", index=False)

df = pd.read_csv("""C:/work/toshiba/consumption_forecasting/data/consumption_dev_data/CER/gas/daily_cer_gas_1.csv""")
df['TimeStamp'] = df['Timestamp']
df['ID'] = df['id']
df = df[['ID','TimeStamp','energy','confidence_score']]
ids = pd.DataFrame(df['ID'].unique(),columns=['ID'])

for i in range(0,len(ids)):
  idi = ids['ID'].iloc[i]
  dfi = df[df['ID'] == idi]
  dfi.to_csv(path + """/algorithm/data/platform/consumption_history_store/daily/GAS_ID_""" + str(idi) + """.csv""", index=False)





ch = pd.read_csv(path + """/data/platform/consumption_history_store/ELEC_ID_1491.csv""")
ch = ch.head(96)
ch.to_csv(path + """/data/platform/consumption_history_store/ELEC_ID_1491.csv""",index=False)