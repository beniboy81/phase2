from profile import Profile
import ancillary_defs as ad

# run setup
def run(runfile):
    with open(runfile, "r") as rnf:
        exec(rnf.read())

run("C:/work/toshiba/consumption_forecasting/algorithm/phase2/code/dev/setup.py")


class Defaults(Profile):
    """This class defines a users default consumption characteristic values. These defaults can we estimated
       daily/weekly/annual consumption values or distributions of consumption across the months of the year, days
       of the week and half hours of the day.
       Defaults for these characterisitcs are generally produced at overall and seasonal level and are different
       for domestic and sme consumers. The default values are derived from lookup files in the platform, in
       'defaults_store'
    """
    def __init__(self, ID, FUEL):
        """Inheritance of the consumer type from the profile class getCustType method. Fuel is a parameter which is passed
        in the main console.
        """
        Profile.__init__(self, ID)
        self.FUEL = FUEL
        self.CONSUMER = self.getCustType()

        # We build a dictionary that contains the defaults for a user/consumer type/fuel combination.
        # The defaults are read in from the defaults_store. The coefficients are distributions of
        # consumption for 48 half hour in a day, 7 days in a week and 12 months in a year and are produced
        # at combinations of overall, season and day. EAC is estimated annual consumption and is a single
        # value. EWC and EDC are straightforward functions of this. The Dictionary 'Dd' stores these defaults
        # in memory once the class is instantiated.

        # make dictionary of defaults
        self.Dd = {}
        
        # EAC, EWC & EDC
        self.eac_0_0 = pd.read_csv(path + """/data/platform/defaults_store/eac.csv""")
        self.Dd['eac_0_0'] = int(self.eac_0_0[(self.eac_0_0['consumer'] == self.CONSUMER) & (self.eac_0_0['fuel'] == self.FUEL)]['value'])
        self.Dd['edc_0_0'] = self.Dd['eac_0_0'] / 365
        self.Dd['ewc_0_0'] = self.Dd['edc_0_0'] * 7

        # day default coefficients
        self.day = pd.read_csv(path + """/data/platform/defaults_store/day.csv""")
        for s in range(0,4):
            self.Dd['h_0_'+str(s)] = self.day[(self.day['fuel'] == self.FUEL) & (self.day['consumer'] == self.CONSUMER) & (self.day['season'] == s)]
        # week default coefficients
        self.week = pd.read_csv(path + """/data/platform/defaults_store/week.csv""")
        for s in range(0,4):
            self.Dd['d_0_'+str(s)] = self.week[(self.week['fuel'] == self.FUEL) & (self.week['consumer'] == self.CONSUMER) & (self.week['season'] == s)]
        # year default coefficients
        self.year = pd.read_csv(path + """/data/platform/defaults_store/year.csv""")
        self.Dd['m_0_0'] = self.year[(self.year['fuel'] == self.FUEL) & (self.year['consumer'] == self.CONSUMER)]

        ad.garbage_collect(self.day)
        ad.garbage_collect(self.week)
        ad.garbage_collect(self.year)

    def writeDefaultsToDefaults(self):
        """This method writes the default values for the user to 'defaults' in the platform. This is a method of the class
        because we may not want to write defaults, preferring to use the 'Dd' dictionary. For consumption characteristics
        it is likely that these will be written to a database of file location for posterity but for running a forecast
        it is not necessary to do this
        """
        self.Dd['eacdf_0_0'] = pd.DataFrame([self.Dd['eac_0_0']], columns=['eac_0_0'])
        self.Dd['edcdf_0_0'] = pd.DataFrame([self.Dd['edc_0_0']], columns=['edc_0_0'])
        self.Dd['ewcdf_0_0'] = pd.DataFrame([self.Dd['ewc_0_0']], columns=['ewc_0_0'])

        self.Dd['eacdf_0_0'].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_eac_0_0.csv', index=False)
        self.Dd['edcdf_0_0'].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_edc_0_0.csv', index=False)
        self.Dd['ewcdf_0_0'].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_ewc_0_0.csv', index=False)

        for s in range(0,4):
            s = str(s)
            self.Dd['h_0_'+s].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_h_0_'+s+'.csv', index=False)
            self.Dd['d_0_'+s].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_d_0_'+s+'.csv', index=False)
        self.Dd['m_0_0'].to_csv(path + '/data/platform/defaults/' + str(self.FUEL) + '_m_0_0.csv', index=False)

#D = Defaults(ID,FUEL)
#print(D.day)
#print(D.week)
#print(D.year)






